# https://superuser.com/questions/1397991/running-alpine-linux-on-qemu-arm-guests

# This is how to get QEMU_EFI-pflash.raw: Get the Fedora edk2 package and extract the file from the RPM package...
# * https://github.com/NixOS/nixpkgs/issues/175202#issuecomment-1140486268
# * https://fedora.pkgs.org/36/fedora-updates-aarch64/edk2-aarch64-20220221gitb24306f15daa-4.fc36.noarch.rpm.html
# Maybe this lands in 22.10, so there's native NixOS support: https://github.com/NixOS/nixpkgs/pull/173110

# For ARM, use: https://fedora.pkgs.org/36/fedora-x86_64/edk2-arm-20220221gitb24306f15daa-2.fc36.noarch.rpm.html

alpine-branch = v3.15
alpine-version = 3.15.4

.PHONY: run-installed
run-installed: alpine-aarch64.qcow2
	qemu-system-aarch64 \
	  -M virt -m 4G -cpu cortex-a57 \
	  -smp cpus=8 \
	  -drive if=pflash,format=raw,readonly=on,file=xxx/usr/share/edk2/aarch64/QEMU_EFI-pflash.raw \
	  -drive file='$<' \
	  -nographic

# https://unix.stackexchange.com/a/623044
# This will take a veeery long time to boot. Be patient.
# Login as root without a password. Then run `setup-alpine` ...
.PHONY: run-installer
run-installer: alpine-aarch64.qcow2 alpine-virt-$(alpine-version)-aarch64.iso
	qemu-system-aarch64 \
	  -M virt -m 2G -cpu cortex-a57 \
	  -drive if=pflash,format=raw,readonly=on,file=xxx/usr/share/edk2/aarch64/QEMU_EFI-pflash.raw \
	  -drive format=raw,readonly=on,file=alpine-virt-$(alpine-version)-aarch64.iso \
	  -drive file='$<' \
	  -nographic

# https://stackoverflow.com/a/49930882
.PHONY: run-installer-armhf
run-installer-armhf: alpine-armhf.qcow2 alpine-virt-$(alpine-version)-armhf.iso
	qemu-system-arm \
	  -M virt -m 2G -cpu cortex-a15 \
	  -pflash yyy/usr/share/edk2/arm/QEMU_EFI-pflash.raw \
	  -cdrom alpine-virt-$(alpine-version)-armhf.iso \
	  -drive file='$<' \
	  -boot d \
	  -nographic

# https://stackoverflow.com/a/49930882
.PHONY: run-installer-armv7
run-installer-armv7: alpine-armv7.qcow2 alpine-virt-$(alpine-version)-armv7.iso
	qemu-system-arm \
	  -M virt -m 2G -cpu cortex-a15 \
	  -pflash yyy/usr/share/edk2/arm/QEMU_EFI-pflash.raw \
	  -cdrom alpine-virt-$(alpine-version)-armv7.iso \
	  -drive file='$<' \
	  -boot d \
	  -nographic

.PHONY: run-netboot
run: alpine-aarch64.qcow2 vmlinuz-lts-$(alpine-branch) initramfs-lts-$(alpine-branch)
	qemu-system-aarch64 \
	  -M virt -m 2G -cpu cortex-a57 \
	  -kernel vmlinuz-lts-$(alpine-branch) -initrd initramfs-lts-$(alpine-branch) \
	  -append "console=ttyAMA0 ip=dhcp alpine_repo=http://dl-cdn.alpinelinux.org/alpine/$(alpine-branch)/main/" \
	  -drive if=none,file='$<',format=qcow2,id=hd \
	  -device virtio-blk-pci,drive=hd \
	  -nographic

# echo "The default boot selection will start in"
# Image console=ttyAMA0 root=/dev/vda2 rootwait rootfstype=ext4 rw verbose debug uefi_debug ignore_loglevel

# https://unix.stackexchange.com/questions/682733/cannot-boot-aarch64-alpine-linux-on-mac-os-using-qemu-6-2-0

# QEMU_EFI.fd:
# 	curl -Lo '$@' 'https://releases.linaro.org/components/kernel/uefi-linaro/latest/release/qemu64/$@'

QEMU_EFI.img:
	curl -Lo - 'https://releases.linaro.org/components/kernel/uefi-linaro/latest/release/qemu64/$@.gz' | gunzip > '$@'

# QEMU_EFI.img:
# 	curl -Lo - https://snapshots.linaro.org/components/kernel/leg-virt-tianocore-edk2-upstream/latest/QEMU-AARCH64/RELEASE_GCC5/QEMU_EFI.img.gz | gunzip > '$@'

alpine-virt-$(alpine-version)-aarch64.iso:
	curl -Lo '$@' 'https://dl-cdn.alpinelinux.org/alpine/$(alpine-branch)/releases/aarch64/$@'

alpine-virt-$(alpine-version)-armhf.iso:
	curl -Lo '$@' 'https://dl-cdn.alpinelinux.org/alpine/$(alpine-branch)/releases/armhf/$@'

alpine-virt-$(alpine-version)-armv7.iso:
	curl -Lo '$@' 'https://dl-cdn.alpinelinux.org/alpine/$(alpine-branch)/releases/armv7/$@'

vmlinuz-lts-$(alpine-branch):
	curl -Lo '$@' 'https://dl-cdn.alpinelinux.org/alpine/$(alpine-branch)/releases/aarch64/netboot/vmlinuz-lts'

initramfs-lts-$(alpine-branch):
	curl -Lo '$@' 'https://dl-cdn.alpinelinux.org/alpine/$(alpine-branch)/releases/aarch64/netboot/initramfs-lts'

alpine-aarch64.qcow2: image-size = 32G
alpine-armv7.qcow2: image-size = 32G
alpine-armhf.qcow2: image-size = 32G

%.qcow2:
	qemu-img create -f qcow2 -o nocow=on '$@' '$(image-size)'
